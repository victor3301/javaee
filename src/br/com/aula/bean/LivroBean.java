package br.com.aula.bean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import br.com.aula.dao.LivroDAO;
import br.com.aula.model.Livro;

@ManagedBean
public class LivroBean {
	LivroDAO lDao;
	private Livro livro;
	
	public LivroBean() {
		livro = new Livro();
		lDao = new LivroDAO();
	}
	
	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public void cadastrarLivro() {
		lDao = new LivroDAO();
		lDao.salvar(livro);
		livro = new Livro();
		FacesContext.getCurrentInstance().addMessage(FacesMessage.FACES_MESSAGES, new FacesMessage("Cadastrado com Sucesso!"));
	}
	
	public void buscarPorId() {
		lDao = new LivroDAO();
		Livro lBusca = lDao.buscarPorId(livro.getId());
		livro = lBusca;
		System.out.println(lDao.listar());
		if(lBusca == null) {
			FacesContext.getCurrentInstance().addMessage(FacesMessage.FACES_MESSAGES, new FacesMessage("Livro n�o Encontrado!"));
		}
	}
	
	public void listarLivros() {
		lDao = new LivroDAO();
		List<Livro> lBusca = lDao.listar();
		System.out.println(lDao.listar());
		if(lBusca == null) {
			FacesContext.getCurrentInstance().addMessage(FacesMessage.FACES_MESSAGES, new FacesMessage("Livro n�o Encontrado!"));
		}
	}
}
