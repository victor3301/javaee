package br.com.aula.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import br.com.aula.model.Usuario;
import br.com.aula.util.JPAUtil;

public class UsuarioDAO implements InterfaceUsuarioDAO<Usuario>{

	@Override
	public void salvarUsuario(Usuario entidade) {
		EntityManager em = JPAUtil.getEntityManager();
		try{
			em.getTransaction().begin();
			if(entidade.getId() == null) {
				em.persist(entidade);
			}else {
				em.merge(entidade);
			}
			em.getTransaction().commit();
		}finally {
			em.close();
		}
	}

	@Override
	public void removerUsuario(int id) {
		EntityManager em = JPAUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			
			Usuario Usuario = em.find(Usuario.class, id);
			
			em.remove(Usuario);
			
			em.getTransaction().commit();
		}finally {
			em.close();
		}
		
	}

	@Override
	public Usuario buscarUsuarioPorId(int id) {
		EntityManager em = JPAUtil.getEntityManager();
		try {
			return em.find(Usuario.class, id);
		}finally {
			em.close();
		}
	}

	@Override
	public List<Usuario> listarUsuario() {
		List<Usuario> Usuarios = new ArrayList<Usuario>();
		EntityManager em = JPAUtil.getEntityManager();
		try {
			Usuarios = em.createQuery("from " + Usuario.class.getSimpleName(), Usuario.class).getResultList();
			return Usuarios;
		}finally {
			em.close();
		}
	}




}
