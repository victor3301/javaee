package br.com.aula.dao;

import java.util.List;

public interface InterfaceUsuarioDAO<T> {

	public void salvarUsuario(T entidade);
	public void removerUsuario(int id);
	public T buscarUsuarioPorId(int id);
	public List<T> listarUsuario();
}
